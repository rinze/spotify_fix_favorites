# Introduction

For a while, when saving an album as favorite on Spotify, all its tracks were 
also saved as favorites individually, which didn't make much sense.

This script fixes that. It scans all the favorite tracks for our user, checks 
how many songs are in the corresponding albums and removes the songs from the 
favorite list if there are too many favorites.

# How to use

1. Go to https://developer.spotify.com/dashboard and click on _Create app_. Use 
   any name / description you want, set `http://localhost` as the _Redirect URL_ 
   and check the _Web API_ box. Agree to the terms of service and save it.
2. Get your credentials and edit them into `creds-orig.sh`. Then source that 
   file so that the environment variables are in your environment.
3. Create and activate a new Python environment like this. You can do this 
   directly in the folder where you have copied the code:
   ```bash
   python -m venv venv
   source venv/bin/activate
   pip install -r requirements.txt
   ```
4. That's it. Run `python spotify_fix_favorites.py` and everything should run. 
   Follow the instructions on screen if it's the first time you run it to 
   complete the authentication.

# Tweaks

If you edit the script you'll see two constants at the top:

* `MIN_SONGS`: minimal number of songs that an album needs to have to worry 
  about it. Defaults to 4.
* `FAVORITE_TO_SONGS_RATIO`: ratio of favorite songs / songs in album to ask the 
  user if the songs should be removed from favorites. Defaults to 0.8, which 
  means that 80 % of songs in the album are in favorites.

# Disclaimer

This script was written in a morning and tested with exactly one user (mine), so 
it might crash in parts I don't know, no warranty whatsoever, etc, etc. Feel 
free to modify it as you like.
